﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerOrderBusinessLayer;


namespace CustomerOrderProj.Controllers
{
    public class OrderController : Controller
    {
        //Displaying Order Index
        public ActionResult Index()
        {
            OrderBusinessLayer orderBusinessLayer = new OrderBusinessLayer();
            List<Order> orders = orderBusinessLayer.Orders.ToList();
            
            return View(orders);
        }

        //HttpGet Method for creating new order
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        //Httppost Method for creating new order
        [HttpPost]
        public ActionResult Create(Order order)
        {
            if (ModelState.IsValid)
            {
                OrderBusinessLayer orderBusinessLayer = new OrderBusinessLayer();
                orderBusinessLayer.AddOrder(order);
                return RedirectToAction("Index");
            }
            return View();
        }

        //Edit method to update order details
        [HttpGet]
        public ActionResult Edit(int id)
        {
                OrderBusinessLayer orderBusinessLayer = new OrderBusinessLayer();
                Order orders = orderBusinessLayer.Orders.Single(or => or.OrderID == id);
                return View(orders);
        }

        //Httppost Method for updating order
        [HttpPost]
        public ActionResult Edit(Order order)
        {
            if (ModelState.IsValid)
            {
                OrderBusinessLayer orderBusinessLayer = new OrderBusinessLayer();
                orderBusinessLayer.UpdateOrder(order);
                return RedirectToAction("Index");
            }
            return View(order);
        }
            

        //public ActionResult Details(int id)
        //{
        //    OrderContext orderContext = new OrderContext();
        //    Order orders = orderContext.orders.Single(or => or.OrderID == id);
        //    return View(orders);
        //}

    }
}
