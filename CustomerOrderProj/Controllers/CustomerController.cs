﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerOrderBusinessLayer;

namespace CustomerOrderProj.Controllers
{
    public class CustomerController : Controller
    {
        //Displaying Customer Index
        public ActionResult Index()
        {
            CustomerBusinessLayer customerBusinessLayer = new CustomerBusinessLayer();
            List<Customer> customers = customerBusinessLayer.Customers.ToList();

            return View(customers);
        }

        //HttpGet Method for adding new customer
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        //HttpPost Method for adding new customer
        [HttpPost]
        public ActionResult Create(Customer customer)
        {
            if (ModelState.IsValid)
            {
                CustomerBusinessLayer customerBusinessLayer = new CustomerBusinessLayer();
                customerBusinessLayer.AddCustomer(customer);
                return RedirectToAction("Index");
            }
            return View();
        }
        
        //Edit method to update customer details
        [HttpGet]
        public ActionResult Edit(int id)
        {
            CustomerBusinessLayer customerBusinessLayer = new CustomerBusinessLayer();
            Customer customers = customerBusinessLayer.Customers.Single(cust => cust.CustomerID == id);
            return View(customers);
        }

        //Httppost Method for updating order
        [HttpPost]
        public ActionResult Edit(Customer customer)
        {
            if (ModelState.IsValid)
            {
                CustomerBusinessLayer customerBusinessLayer = new CustomerBusinessLayer();
                customerBusinessLayer.UpdateCustomer(customer);
                return RedirectToAction("Index");
            }
            return View(customer);
        }

    }
}
