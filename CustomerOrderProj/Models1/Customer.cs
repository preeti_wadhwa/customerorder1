﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerOrderProj.Models
{
    [Table("tblCustomer")]
    public class Customer
    {
        public int CustomerID { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string City { get; set; }
        public List<Order> Orders { get; set; }

    }
}