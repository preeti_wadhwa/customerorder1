﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using CustomerOrderProj.Models;

namespace CustomerOrderProj.Models
{
    public class OrderContext : DbContext
    {
        public DbSet<Order> orders { get; set; }
        public DbSet<Customer> customers { get; set; }
    }
}