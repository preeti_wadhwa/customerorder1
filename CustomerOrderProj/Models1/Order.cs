﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CustomerOrderProj.Models
{
    [Table("tblOrder")]
    public class Order
    {
        [Key]
        public int OrderID { get; set; }
        public string OrderName { get; set; }
        public DateTime Date { get; set; }
        public int CustomerID { get; set; }
    }
}