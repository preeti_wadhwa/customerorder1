﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CustomerOrderBusinessLayer
{
    public class Order
    {
        public int OrderID { get; set; }
        [Required]
        public string OrderName { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public int CustomerID { get; set; }
    }
}
