﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace CustomerOrderBusinessLayer
{
    public class OrderBusinessLayer
    {
        //Business logic for retrieveing list of orders
        public IEnumerable<Order> Orders 
        {
            get 
            {
                string connectionString = ConfigurationManager.ConnectionStrings["CustomerOrder"].ConnectionString;

                List<Order> orders = new List<Order>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spGetAllOrders", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        Order order = new Order();
                        order.OrderID = Convert.ToInt32(rdr["OrderID"]);
                        order.OrderName = rdr["OrderName"].ToString();
                        order.Date = Convert.ToDateTime(rdr["Date"]);
                        order.CustomerID = Convert.ToInt32(rdr["CustomerID"]);

                        orders.Add(order);
                    }
                }
                return orders;
            }
        }
        
        //Business logic for adding new order
        public void AddOrder(Order order)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CustomerOrder"].ConnectionString;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("spAddOrder", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter parmName = new SqlParameter();
                parmName.ParameterName = "@OrderName";
                parmName.Value = order.OrderName;
                cmd.Parameters.Add(parmName);

                SqlParameter parmDate = new SqlParameter();
                parmDate.ParameterName = "@Date";
                parmDate.Value = order.Date;
                cmd.Parameters.Add(parmDate);

                SqlParameter parmCustomerID = new SqlParameter();
                parmCustomerID.ParameterName = "@CustomerID";
                parmCustomerID.Value = order.CustomerID;
                cmd.Parameters.Add(parmCustomerID);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        //Business logic for updating order
        public void UpdateOrder(Order order)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CustomerOrder"].ConnectionString;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("spUpdateOrder", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter parmID = new SqlParameter();
                parmID.ParameterName = "@ID";
                parmID.Value = order.OrderID;
                cmd.Parameters.Add(parmID);
                
                SqlParameter parmName = new SqlParameter();
                parmName.ParameterName = "@OrderName";
                parmName.Value = order.OrderName;
                cmd.Parameters.Add(parmName);

                SqlParameter parmDate = new SqlParameter();
                parmDate.ParameterName = "@Date";
                parmDate.Value = order.Date;
                cmd.Parameters.Add(parmDate);

                SqlParameter parmCustomerID = new SqlParameter();
                parmCustomerID.ParameterName = "@CustomerID";
                parmCustomerID.Value = order.CustomerID;
                cmd.Parameters.Add(parmCustomerID);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
    public class CustomerBusinessLayer
    {
        //Business logic for retrieveing list of customers
        public IEnumerable<Customer> Customers
        {
            get
            {
                string connectionString = ConfigurationManager.ConnectionStrings["CustomerOrder"].ConnectionString;

                List<Customer> customers = new List<Customer>();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("spGetAllCustomers", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        Customer customer = new Customer();
                        customer.CustomerID = Convert.ToInt32(rdr["CustomerID"]);
                        customer.Name = rdr["Name"].ToString();
                        customer.Gender = rdr["Gender"].ToString();
                        customer.City = rdr["City"].ToString();

                        customers.Add(customer);
                    }
                }
                return customers;
            }
        }

        //Business logic for adding new customer
        public void AddCustomer(Customer customer)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CustomerOrder"].ConnectionString;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("spAddCustomer", con);
                cmd.CommandType = CommandType.StoredProcedure;
               
                SqlParameter parmName = new SqlParameter();
                parmName.ParameterName = "@Name";
                parmName.Value = customer.Name;
                cmd.Parameters.Add(parmName);

                SqlParameter parmGender = new SqlParameter();
                parmGender.ParameterName = "@Gender";
                parmGender.Value = customer.Gender;
                cmd.Parameters.Add(parmGender);

                SqlParameter parmCity = new SqlParameter();
                parmCity.ParameterName = "@City";
                parmCity.Value = customer.City;
                cmd.Parameters.Add(parmCity);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        //Business logic for updating customer
        public void UpdateCustomer(Customer customer)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CustomerOrder"].ConnectionString;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("spUpdateCustomer", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter parmID = new SqlParameter();
                parmID.ParameterName = "@ID";
                parmID.Value = customer.CustomerID;
                cmd.Parameters.Add(parmID);

                SqlParameter parmName = new SqlParameter();
                parmName.ParameterName = "@Name";
                parmName.Value = customer.Name;
                cmd.Parameters.Add(parmName);

                SqlParameter parmGender = new SqlParameter();
                parmGender.ParameterName = "@Gender";
                parmGender.Value = customer.Gender;
                cmd.Parameters.Add(parmGender);

                SqlParameter parmCity = new SqlParameter();
                parmCity.ParameterName = "@City";
                parmCity.Value = customer.City;
                cmd.Parameters.Add(parmCity);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
